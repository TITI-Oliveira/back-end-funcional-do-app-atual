export interface User {
  id: string,
  name: string,
  email: string,
  password: string,
  profilePic: any,
  keepLogged: boolean
}