export interface Order {
  id: string,
  product: string,
  price: number,
  reward_value: number,
  location_from: string,
  location_final: string,
  status: string,
  statusMessage: string,
  buyer: string,
  bringer: string,
  arrival_expectation: Date,
  arrival_date: Date
}