import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './home/login/login.component';
import { CadastroComponent } from './home/cadastro/cadastro.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Bring2meComponent } from './core/bring2me/bring2me.component';
import { Bring2someoneComponent } from './core/bring2someone/bring2someone.component';
import { PedidoComponent } from './core/pedido/pedido.component';
import { PerfilComponent } from './core/perfil/perfil.component';
import { DetalhesComponent } from './core/bring2me/abertos/detalhes/detalhes.component';
import { AuthGuard } from './guards/auth.guard';
import { AdmloginComponent } from './admin/admlogin/admlogin.component';
import { AdmdashboardComponent } from './admin/admdashboard/admdashboard.component';
import { UserComponent } from './admin/admreports/user/user.component';
import { OrderComponent } from './admin/admreports/order/order.component';
import { AdminGuard } from './guards/admin.guard';


const routes: Routes = [
    { path: '', component: HomeComponent},
    { path: 'login', component: LoginComponent},
    { path: 'cadastro', component: CadastroComponent},
    { path: 'home', component: DashboardComponent, canActivate: [AuthGuard] },
    { path: 'bring2me', component: Bring2meComponent, canActivate: [AuthGuard] },
    { path: 'bring2someone', component: Bring2someoneComponent, canActivate: [AuthGuard] },
    { path: 'pedido', component: PedidoComponent, canActivate: [AuthGuard]},
    { path: 'bring2me/order/:id', component: DetalhesComponent, canActivate: [AuthGuard] },
    { path: 'perfil', component: PerfilComponent },
    { path: 'admin', component: AdmloginComponent },
    { path: 'dashboard', component: AdmdashboardComponent, canActivate: [AdminGuard] },
    { path: 'dashboard/users', component: UserComponent, canActivate: [AdminGuard] },
    { path: 'dashboard/orders', component: OrderComponent, canActivate: [AdminGuard] }
    // { path: 'perfil/pagamento', component: PagamentoComponent}
    // { path: 'bring2me/abertos', component: AbertosComponent },
    // { path: 'bring2me/espera', component: EsperaComponent},
    // { path: 'bring2me/concluidos', component: ConcluidoComponent},
    // { path: 'bring2someone', component: Bring2someoneComponent},
    // { path: 'pedido', component: PedidoB2mComponent},
    // { path: 'perfil', component: PerfilComponent}
  ];
  
  @NgModule({
    imports: [
      RouterModule.forRoot(routes)
    ],
    exports: [
      RouterModule
    ],
    providers: [AuthGuard, AdminGuard]
  })
  
  export class AppRoutingModule {}