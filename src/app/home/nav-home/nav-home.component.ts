import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
declare var $:any;

@Component({
  selector: 'nav-home',
  templateUrl: './nav-home.component.html',
  styleUrls: ['./nav-home.component.css']
})
export class NavHomeComponent implements OnInit {
  private isAuth: boolean;

  constructor(private userService: UserService) { }

  ngOnInit() {
    $('.sidenav').sidenav();
    this.isAuth = this.userService.getAuth();
  }

}
