import { Component, OnInit } from '@angular/core';
import { FounderService } from '../../services/founder.service';

import { Founder } from '../../models/founder.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'intro-home',
  templateUrl: './intro-home.component.html',
  styleUrls: ['./intro-home.component.css']
})
export class IntroHomeComponent implements OnInit {
  founders: Founder[] = [];
  private foundersSub: Subscription;

  constructor(private founderService: FounderService) { }

  ngOnInit() {
    this.founderService.getFounders();
    this.foundersSub = this.founderService.getFoundersListener()
    .subscribe((founders: Founder[]) => {
      this.founders = founders;
    });
  }

}
