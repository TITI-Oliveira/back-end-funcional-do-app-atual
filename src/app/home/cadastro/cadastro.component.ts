import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { PasswordValidation } from '../../validators/PasswordMatch.validators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {
  logo = "./../../../assets/images/logooutlined.png";
  registerForm: FormGroup;

  constructor(private userService: UserService, private router: Router) {
    const isAuth = this.userService.getAuth();
    if (isAuth) this.router.navigate(['/home']);
  }

  ngOnInit() {
    this.registerForm = new FormGroup({
        'name': new FormControl(
          null,
          { validators: [Validators.required, Validators.minLength(7)] }
        ),
        'email': new FormControl(
          null,
          { validators: [Validators.required, Validators.email] }
        ),
        'password': new FormControl(
          null,
          { validators: [Validators.required, Validators.minLength(6) ] }
        ),
        'passwordConf': new FormControl(
          null,
          { validators: [Validators.required] }
        )
      },
      { validators: [PasswordValidation.MatchPassword] 
    });
  }

  get name() {
    return this.registerForm.get('name');
  }

  get email() {
    return this.registerForm.get('email');
  }

  get password() {
    return this.registerForm.get('password');
  }

  get passwordConf() {
    return this.registerForm.get('passwordConf');
  }

  onSubmit() {
    if (this.registerForm.invalid) return;
    this.userService.signup({ ...this.registerForm.value });
    // this.registerForm.reset();
  }

}
