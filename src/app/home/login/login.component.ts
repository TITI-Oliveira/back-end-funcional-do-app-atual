import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  logo = "./../../../assets/images/logooutlined.png";
  loginForm: FormGroup;

  constructor(private userService: UserService, private router: Router) {
    const isAuth = this.userService.getAuth();
    if (isAuth) this.router.navigate(['/home']);
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      'email': new FormControl(
        null,
        { validators: [Validators.required, Validators.email] }
      ),
      'password': new FormControl(
        null,
        { validators: [Validators.required] }
      ),
      'keepLogged': new FormControl(true)
    });
  }

  get email() {
    return this.loginForm.get('email');
  }

  get password() {
    return this.loginForm.get('password');
  }

  onLogin() {
    // console.log(this.loginForm.value);
    if (this.loginForm.invalid) return;
    this.userService.login({ ...this.loginForm.value });
    this.loginForm.reset();
  }

}
