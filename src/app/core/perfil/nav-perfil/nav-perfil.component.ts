import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
declare var $:any;

@Component({
  selector: 'nav-perfil',
  templateUrl: './nav-perfil.component.html',
  styleUrls: ['./nav-perfil.component.css']
})
export class NavPerfilComponent implements OnInit {

  constructor(private userService: UserService) { }

  ngOnInit() {
    $('.sidenav').sidenav();
  }

  onLogout() {
    this.userService.logout();
  }

}
