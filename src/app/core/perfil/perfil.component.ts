import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user.model';
import { FormGroup, Validators, FormControl } from '@angular/forms';

declare var M:any;

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  profile: User;
  profileForm: FormGroup;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.profileForm = new FormGroup({
      'name': new FormControl(
        null,
        { validators: [Validators.required, Validators.minLength(7)] }
      ),
      'email': new FormControl(
        null,
        { validators: [Validators.required, Validators.email] }
      ),
      'profilePic': new FormControl(null)
    })
    this.userService.getProfile().then(profile => {
      this.profile = profile
      this.profileForm.get('name').setValue(this.profile.name);
      this.profileForm.get('email').setValue(this.profile.email);
    });

  }

  onImagePicked(event: Event) {
    console.log('TESTE');
    const file = (event.target as HTMLInputElement).files[0];
    this.profileForm.patchValue({'profilePic': file});
    this.profileForm.get('profilePic').updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.profile.profilePic = reader.result;
    };
    console.log(this.profileForm.value);
    reader.readAsDataURL(file);
  }

  onSaveProfile() {
    if (this.profileForm.invalid) return;
    this.userService.saveProfile({ ...this.profileForm.value });
    M.toast({html: 'Perfil alterado!', classes: 'rounded white deep-purple-text'});
  }

}
