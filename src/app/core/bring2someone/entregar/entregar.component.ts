import { Component, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';
import { Order } from '../../../models/order.model';
import { OrderService } from '../../../services/order.service';

@Component({
  selector: 'entregar',
  templateUrl: './entregar.component.html',
  styleUrls: ['./entregar.component.css']
})
export class EntregarComponent implements OnInit {
  orders: Order[] = [];
  private ordersSub: Subscription;

  teste = "https://loremflickr.com/1600/900";

  constructor(private orderService: OrderService) { }

  ngOnInit() {
  }

  public getOrders() {
    this.orderService.getOrders('2', true);
    // this.ordersSub = this.orderService.getOrdersListener()
    // .subscribe((orders: Order[]) => {
    //   this.orders = orders;
    // });
  }

}
