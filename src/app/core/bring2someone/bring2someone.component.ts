import { Component, OnInit, ViewChild } from '@angular/core';
import { FeedComponent } from './feed/feed.component';
import { EntregarComponent } from './entregar/entregar.component';
declare var $:any;

@Component({
  selector: 'app-bring2someone',
  templateUrl: './bring2someone.component.html',
  styleUrls: ['./bring2someone.component.css']
})
export class Bring2someoneComponent implements OnInit {
  @ViewChild(FeedComponent) feed:FeedComponent;
  @ViewChild(EntregarComponent) entregar:EntregarComponent;

  constructor() { }

  ngOnInit() {
    $('.tabs').tabs();
  }

  onOpenFeed() {
    this.feed.getOrders();
  }

  onOpenEntregar() {
    this.entregar.getOrders();
  }

}