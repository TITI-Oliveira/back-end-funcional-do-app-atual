import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bring2someoneComponent } from './bring2someone.component';

describe('Bring2someoneComponent', () => {
  let component: Bring2someoneComponent;
  let fixture: ComponentFixture<Bring2someoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bring2someoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bring2someoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
