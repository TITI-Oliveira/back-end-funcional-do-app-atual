import { Component, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';
import { Order } from '../../../models/order.model';
import { OrderService } from '../../../services/order.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

declare var M:any;
declare var $:any;

@Component({
  selector: 'feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit {
  // teste =  "./../../assets/images/kumena.jpg";
  orders: Order[] = [];
  proposalForm: FormGroup;
  // private ordersSub: Subscription;

  teste = "https://loremflickr.com/1600/900";

  constructor(private orderService: OrderService) { }

  ngOnInit() {
    $('.modal').modal();
    $('.recusar').click(function() {
      $(this).parent().parent().parent().toggle(500);
    });
    this.getOrders();
    this.proposalForm = new FormGroup({
      'proposal': new FormControl(
        null,
        { validators: [Validators.required] }
      )
    })
  }

  get proposal() {
    return this.proposalForm.get('proposal');
  }

  openProposalDiv(id: string) {
    $(`#${id}`).toggle(500);
    $(`.${id}`).toggle(700);
  }

  closeProposalDiv(id: string) {
    $(`#${id}`).toggle(500);
    $(`.${id}`).toggle(700);
  }

  public getOrders() {
    this.orderService.getAllOrders();
    this.orderService.ordersListener().subscribe((orders: Order[]) => this.orders = orders);
  }

  onMakeProposal(order: Order) {
    if (this.proposalForm.invalid) return
    this.orderService.makeProposal(this.proposal.value, order);
    this.closeProposalDiv(order.id);
    M.toast({html: 'Proposta feita!', classes: 'rounded deep-purple white-text'});
  }

}
