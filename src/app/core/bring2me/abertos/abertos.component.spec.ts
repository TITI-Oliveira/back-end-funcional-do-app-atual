import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbertosComponent } from './abertos.component';

describe('AbertosComponent', () => {
  let component: AbertosComponent;
  let fixture: ComponentFixture<AbertosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbertosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbertosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
