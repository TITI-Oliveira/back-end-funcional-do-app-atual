import { Component, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';
import { Order } from '../../../models/order.model';
import { OrderService } from '../../../services/order.service';

declare var $:any;

@Component({
  selector: 'abertos',
  templateUrl: './abertos.component.html',
  styleUrls: ['./abertos.component.css']
})
export class AbertosComponent implements OnInit {
  teste = "http://lorempixel.com/800/800/";
  orders: Order[] = [];
  private ordersSub: Subscription;

  constructor(private orderService: OrderService) { }

  ngOnInit() {
    $('.dropdown-trigger').dropdown();
    this.getOrders();
  }

  public getOrders() {
    var status = "1";
    this.orderService.getOrders(status);
    // this.orderService.ordersListener().subscribe((orders: Order[]) => this.orders = orders);
    this.ordersSub = this.orderService.ordersListener().subscribe((orders: Order[]) => this.orders = orders);
    // this.ordersSub = this.orderService.getOrdersListener()
    // .subscribe((orders: Order[]) => {
    //   this.orders = orders;
    // });
  }

}
