import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderService } from '../../../../services/order.service';
import { Order } from '../../../../models/order.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

declare var M: any;
declare var $:any;

@Component({
  selector: 'app-detalhes',
  templateUrl: './detalhes.component.html',
  styleUrls: ['./detalhes.component.css']
})
export class DetalhesComponent implements OnInit {
  productLink;
  product;
  price;
  reward_value;
  location_from;
  location_final;
  order: Order;
  orderId: string;
  editForm: FormGroup;

  constructor(private router: Router, private route: ActivatedRoute, private orderService: OrderService) { }

  ngOnInit() {
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems);
    $('.fixed-action-btn').floatingActionButton();
    $('.tooltipped').tooltip();
    $('.modal').modal();
    this.editForm = new FormGroup({
      'productLink': new FormControl(
        null,
        { validators: [Validators.required] }
      ),
      'product': new FormControl(
        null,
        { validators: [Validators.required, Validators.minLength(4)] }
      ),
      'price': new FormControl(
        null,
        { validators: [Validators.required] }
      ),
      'reward_value': new FormControl(
        null,
        { validators: [Validators.required] }
      ),
      'location_from': new FormControl(
        null,
        { validators: [Validators.required] }
      ),
      'location_final': new FormControl(
        null,
        { validators: [Validators.required] }
      )
    });
    this.route.params.subscribe(params => this.orderId = params.id);
    this.orderService.getOrder(this.orderId).then(order => {
      this.order = order;
      // console.log(order);
      switch (this.order.status) {
        case '1':
          this.order.statusMessage = 'Aguardando propostas';
          break;
      
        default:
          break;
      }
      this.editForm.get('product').setValue(order.product);
      this.editForm.get('price').setValue(order.price);
      this.editForm.get('reward_value').setValue(order.reward_value);
      this.editForm.get('location_from').setValue(order.location_from);
      this.editForm.get('location_final').setValue(order.location_final);
    });
  }

  onSaveOrder() {
    let formVal = { _id: this.order.id, ...this.editForm.value, status: this.order.status };
    // let defaultVal = { ...this.order };
    // delete formVal.productLink;
    // delete defaultVal.statusMessage;
    // let isDefault = Object.is(formVal, defaultVal);
    // console.log(isDefault);
    // if (isDefault) return
    // console.log(formVal);
    this.orderService.editOrder({ ...formVal }, this.order.id).then(order => {
      this.order = order;
      switch (this.order.status) {
        case '1':
          this.order.statusMessage = 'Aguardando propostas';
          break;
      
        default:
          break;
      }
    });
  }

  onDeleteOrder() {
    this.orderService.deleteOrder(this.order.id).then(message => {
      if (message) this.router.navigate(['/bring2me']);
    });
  }

  onAcceptProposal(proposalId: string) {
    console.log(proposalId);
  }

  onDenyProposal(proposalId: string) {
    console.log(proposalId);
  }

  // onDeleteOrder(id) {
  //   console.log("TESTE");
  //   this.orderService.deleteOrder(id);
  // }

  // onEditOrder(id) {
  //   // console.log(form.value);
  //   // const order: Order = {
  //   //   id,
  //   //   ...this.orderForm.value
  //   // };
  //   // this.orderService.editOrder(order);
  // }

}
