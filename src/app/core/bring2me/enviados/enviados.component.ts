import { Component, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';
import { Order } from '../../../models/order.model';
import { OrderService } from '../../../services/order.service';

@Component({
  selector: 'enviados',
  templateUrl: './enviados.component.html',
  styleUrls: ['./enviados.component.css']
})
export class EnviadosComponent implements OnInit {
  orders: Order[] = [];
  private ordersSub: Subscription;

  constructor(private orderService: OrderService) { }

  ngOnInit() {
  }

  public getOrders() {
    var status = "2";
    this.orderService.getOrders(status);
    // this.ordersSub = this.orderService.getOrdersListener()
    // .subscribe((orders: Order[]) => {
    //   this.orders = orders;
    // });
  }

}
