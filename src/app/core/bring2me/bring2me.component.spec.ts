import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bring2meComponent } from './bring2me.component';

describe('Bring2meComponent', () => {
  let component: Bring2meComponent;
  let fixture: ComponentFixture<Bring2meComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bring2meComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bring2meComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
