import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
declare var $:any;

import { OrderService } from '../../services/order.service';
import { AbertosComponent } from './abertos/abertos.component';
import { EnviadosComponent } from './enviados/enviados.component';
import { ConcluidosComponent } from './concluidos/concluidos.component';
import { Order } from '../../models/order.model';

@Component({
  selector: 'app-bring2me',
  templateUrl: './bring2me.component.html',
  styleUrls: ['./bring2me.component.css']
})
export class Bring2meComponent implements OnInit {
  img = false;
  produtoUrl:any;
  @ViewChild(AbertosComponent) abertos:AbertosComponent;
  @ViewChild(EnviadosComponent) enviados:EnviadosComponent;
  @ViewChild(ConcluidosComponent) concluidos:ConcluidosComponent;
  newOrderForm: FormGroup;

  constructor(public orderService: OrderService) { }

  ngOnInit() {
    $('.tabs').tabs();
    $('.modal').modal();
    $('.fixed-action-btn').floatingActionButton();
    this.newOrderForm = new FormGroup({
      'productLink': new FormControl(
        null,
        { validators: [Validators.required] }
      ),
      'product': new FormControl(
        null,
        { validators: [Validators.required, Validators.minLength(4)] }
      ),
      'price': new FormControl(
        null,
        { validators: [Validators.required] }
      ),
      'reward_value': new FormControl(
        null,
        { validators: [Validators.required] }
      ),
      'location_from': new FormControl(
        null,
        { validators: [Validators.required] }
      ),
      'location_final': new FormControl(
        null,
        { validators: [Validators.required] }
      )
    });
  }

  get productLink() {
    return this.newOrderForm.get('productLink');
  }

  get product() {
    return this.newOrderForm.get('product');
  }

  get price() {
    return this.newOrderForm.get('price');
  }

  get reward_value() {
    return this.newOrderForm.get('reward_value');
  }

  get location_from() {
    return this.newOrderForm.get('location_from');
  }

  get location_final() {
    return this.newOrderForm.get('location_final');
  }

  onAddOrder() {
    if (this.newOrderForm.invalid) return;
    // const order: Order = {
    //   ...this.newOrderForm.value
    // };
    // this.orderService.addOrder({ ...this.newOrderForm.value });
    this.orderService.addOrder({ ...this.newOrderForm.value });
    this.newOrderForm.reset();
  }

  public changed(event:any) {
    this.img = true;
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
  
      reader.onload = (event:any) => {
        this.produtoUrl = event.target.result;
      }
  
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  openAbertos() {
    this.abertos.getOrders();
  }

  openEnviados() {
    this.enviados.getOrders();
  }

  openConcluidos() {
    this.concluidos.getOrders();
  }

}
