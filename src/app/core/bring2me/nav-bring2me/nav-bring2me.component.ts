import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
declare var $:any;

@Component({
  selector: 'nav-bring2me',
  templateUrl: './nav-bring2me.component.html',
  styleUrls: ['./nav-bring2me.component.css']
})
export class NavBring2meComponent implements OnInit {

  constructor(private userService: UserService) { }

  ngOnInit() {
    $('.sidenav').sidenav();
  }

  onLogout() {
    this.userService.logout();
  }

}
