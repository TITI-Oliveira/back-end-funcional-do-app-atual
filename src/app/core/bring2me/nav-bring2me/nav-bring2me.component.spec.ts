import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavBring2meComponent } from './nav-bring2me.component';

describe('NavBring2meComponent', () => {
  let component: NavBring2meComponent;
  let fixture: ComponentFixture<NavBring2meComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavBring2meComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavBring2meComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
