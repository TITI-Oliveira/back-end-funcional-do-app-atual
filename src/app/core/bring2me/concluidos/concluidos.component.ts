import { Component, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';
import { Order } from '../../../models/order.model';
import { OrderService } from '../../../services/order.service';

@Component({
  selector: 'concluidos',
  templateUrl: './concluidos.component.html',
  styleUrls: ['./concluidos.component.css']
})
export class ConcluidosComponent implements OnInit {
  orders: Order[] = [];
  private ordersSub: Subscription;

  constructor(private orderService: OrderService) { }

  ngOnInit() {}

  public getOrders() {
    var status = "3";
    this.orderService.getOrders(status);
    // this.ordersSub = this.orderService.getOrdersListener()
    // .subscribe((orders: Order[]) => {
    //   this.orders = orders;
    // });
  }

}
