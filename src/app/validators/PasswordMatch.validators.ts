import {AbstractControl} from '@angular/forms';
export class PasswordValidation {

  static MatchPassword(AC: AbstractControl) {
    let senha = AC.get('password').value; // to get value in input tag
    let confirmacaoSenha = AC.get('passwordConf').value; // to get value in input tag
    
    if(senha !== confirmacaoSenha) {
        AC.get('passwordConf').setErrors({ MatchPassword: true })
    } else {
        return null;
    }
  }
}