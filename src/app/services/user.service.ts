import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

import { User } from "../models/user.model";
import { AuthUser } from '../models/authuser.model';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

@Injectable({ providedIn: "root" })
export class UserService {
  private prefix: string = "http://localhost:3000/api/user";
  private token: string;
  private tokenTimer: any;
  private isAuthenticated = new Subject<boolean>();

  constructor(private http: HttpClient, private router: Router) {}

  getToken() {
    return this.token;
  }

  getAuth() {
    if (this.token) return true;
    return false;
  }

  async getProfile() {
    let response = await this.http.get<{ message: string, user: User }>(`${this.prefix}/getProfile`).toPromise();
    return response.user;
  }

  async saveProfile(profile: User) {
    // console.log(profile);
    console.log(profile);
    let response = await this.http.put<{ message: string }>(`${this.prefix}/updateProfile`, profile).toPromise();
    return response;
    // this.http.put(`${this.prefix}/updateProfile`, profile);
  }

  // getAuthStatusListener() {
  //   return this.isAuthenticated.asObservable();
  // }

  signup(user: AuthUser) {
    console.log(user);
    // (`${this.prefix}/signup`, user)
    this.http.post<{ message: string, error: string }>(`${this.prefix}/signup`, user).subscribe(response => {
      console.log(response.message);
    });
    // this.http.post<{ message: string }>(`${this.prefix}/signup`, user);
      // .subscribe(responseData => {
      //   console.log(responseData.message);
      // });
  }

  login(user: AuthUser) {
    // const user: AuthUser = { email, password };
    this.http.post<{ token: string, expiresIn: number }>(`${this.prefix}/login`, user)
      .subscribe(response => {
        this.token = response.token;
        if (this.token) {
          if (response.expiresIn) {
            const now = new Date();
            this.setAuthTimer(10800);
            const expirationDate = new Date(now.getTime() + response.expiresIn *1000);
            this.saveAuthData(this.token, expirationDate);
          } else {
            this.saveAuthData(this.token);
          }
          // this.isAuthenticated.next(true);
          this.router.navigate(['/home']);
        }
      });
  }

  logout() {
    this.clearAuthData();
    this.router.navigate(['/']);
  }

  /**
   * Parte destinada à metodos relacionados a autenticação
   */

  autoAuth() {
    const authInfo = this.getAuthData();
    if (!authInfo) return;
    this.token = authInfo.token;
    if (authInfo.expirationDate) {
      const now = new Date();
      const expiresIn = authInfo.expirationDate.getTime() - now.getTime();
      this.setAuthTimer(expiresIn/1000);
    }
  }

  private saveAuthData(token, expirationDate?) {
    localStorage.setItem('token', token);
    if (expirationDate) localStorage.setItem('expiration', expirationDate.toISOString());
  }

  private clearAuthData() {
    this.token = undefined;
    localStorage.removeItem('token');
    localStorage.removeItem('expiration');
  }

  private getAuthData() {
    const token = localStorage.getItem('token');
    const expirationDate = localStorage.getItem('expiration');
    if (!token) {
        return;
    }
    if (!expirationDate) return { token };
    return { token, expirationDate: new Date(expirationDate) };
  }

  private setAuthTimer(duration: number) {
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 1000);
  }

}