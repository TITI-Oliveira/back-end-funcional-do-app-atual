import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Subject, Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { Order } from '../models/order.model';

@Injectable({providedIn: "root"})
export class OrderService {
  // private orders: Order[] = [];
  private staticOrders: Order[] = [];
  private orders = new Subject<Order[]>();
  private prefix = 'http://localhost:3000/api/order';
  constructor(private http: HttpClient) {}

  // getOrdersListener() {
  //   return this.ordersObservable.asObservable();
  // }

  ordersListener() {
    return this.orders.asObservable();
  }

  async getAllOrders() {
    const response = await this.http.get<{orders: Order[]}>(`${this.prefix}/getAll`).toPromise();
    this.staticOrders = response.orders;
    this.orders.next([...this.staticOrders]);
  }

  async getOrder(id: string) {
    let url = `${this.prefix}/getById/${id}`;
    const response = await this.http.get<{ order: Order }>(url).toPromise();
    return response.order;
  }

  async getOrders(status, isBringer?) {
    let url = `${this.prefix}/getByStatus/${status}`;
    if (isBringer) url = `${this.prefix}/getByStatus/${status}/bringer`
    const response = await this.http.get<{ orders: Order[] }>(url).toPromise();
    this.staticOrders = response.orders;
    this.orders.next([...this.staticOrders]);
  }

  addOrder(order: Order) {
    let ordersSub: Subscription = this.ordersListener().subscribe((orders: Order[]) => this.staticOrders = orders);
    this.http.post<{ message: string, order: Order }>(`${this.prefix}/create`, order).subscribe((response) => {
      this.staticOrders.push(response.order);
      this.orders.next([...this.staticOrders]);
    });
    ordersSub.unsubscribe();
  }

  async editOrder(order: Order, id: string) {
    let response = await this.http.put<{order: Order}>(`${this.prefix}/edit/${id}`, order).toPromise();
    return response.order;
    // this.http.put<{ message: string, order: Order}>(`${this.prefix}/edit`, order);
    // this.http.put(`${this.prefix}/edit/${id}`, order);
  }

  async deleteOrder(id: string) {
     let response = await this.http.delete<{message: string}>(`${this.prefix}/delete/${id}`).toPromise();
     return response.message;
  }
  // editOrder(id: string ,product: string, price: number, reward_value: number, location_from: string, location_final: string, buyer: string) {
  //   const order: Order = {id: null, product, price, reward_value, location_from, location_final, status: null, buyer, bringer: null, arrival_expectation: null, arrival_date: null}; 
  //     this.http.patch<{message: string}>("http://localhost:3000/api/order/edit/"+id, order)
  //       .subscribe(resposeData => {
  //         this.orders.push(order);
  //         this.ordersObservable.next([...this.orders]);
  //       });
  // }

  // deleteOrder(id) {
  //   console.log("TESEEEEE");
  //   this.http.delete("http://localhost:3000/api/order/delete/"+id);
  // }

  async makeProposal(offer, order) {
    const proposal = { offer, order };
    let response = await this.http.put<{ message: string }>(`${this.prefix}/${order.id}/addProposal`, proposal).toPromise();
    console.log(response);
  }

}