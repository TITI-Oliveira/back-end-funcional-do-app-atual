import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Subject } from "rxjs";
import { map } from 'rxjs/operators';

import { Founder } from "../models/founder.model";

@Injectable({providedIn: "root"})
export class FounderService {
  private founders: Founder[] = [];
  private foundersObservable = new Subject<Founder[]>();

  constructor(private http: HttpClient) {}

  getFoundersListener() {
    return this.foundersObservable.asObservable();
  }

  getFounders() {
    this.http
    .get<{ message: string; founders: any }>(
      "http://localhost:3000/api/founders"
    )
    .subscribe(data => {
      this.founders = data.founders;
      this.foundersObservable.next([...this.founders]);
    });
  }

}