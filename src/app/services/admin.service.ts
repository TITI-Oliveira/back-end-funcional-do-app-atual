import { Injectable } from '@angular/core';
import { AuthUser } from '../models/authuser.model';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { User } from '../models/user.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  prefix = "http://localhost:3000/api/admin";
  token;
  tokenTimer: any;

  constructor(private http: HttpClient, private router: Router) { }

  async searchUsers(data: {initialDate: Date, finalDate: Date}) {
    // console.log(data.initialDate.toISOString());
    // console.log(data.finalDate.toISOString());
    let response = await this.http.post<{message: string, users: any[]}>(`${this.prefix}/reports/users`, data).toPromise();
    // console.log(response);
    return response.users;
  }

  async searchOrders(data: {initialDate: Date, finalDate: Date}) {
    // console.log(data.initialDate.toISOString());
    // console.log(data.finalDate.toISOString());
    console.log(data);
    let response = await this.http.post<{message: string, orders: any[]}>(`${this.prefix}/reports/orders`, data).toPromise();
    // console.log(response);
    return response.orders;
  }
  async searchOrdersByProductName(data: {productName: string}) {
    console.log(data);
    let response = await this.http.post<{message: string, orders: any[]}>(`${this.prefix}/reports/ordersByProduct`, data).toPromise();
    return response.orders
  }

  async getUsersFromLastWeek() {
    let response = await this.http.get<{ message: string, usersSum: number }>(`${this.prefix}/sum/users`).toPromise();
    return response.usersSum;
  }

  async getOrdersFromLastWeek() {
    let response = await this.http.get<{ message: string, ordersSum: number }>(`${this.prefix}/sum/orders`).toPromise();
    return response.ordersSum;
  }

  getToken() {
    return this.token
  }

  getAuth() {
    if (this.token) return true;
    return false;
  }

  async login(admin: AuthUser) {
    let response = await this.http.post<{ message: string, token: string, expiresIn: number }>(`${this.prefix}/login`, admin).toPromise();
    if (response.token) {
      this.token = response.token;
      const now = new Date();
      this.setAuthTimer(21600);
      const expirationDate = new Date(now.getTime() + response.expiresIn * 1000);
      this.saveAuthData(this.token, expirationDate);
      return true;
    }
  }

  logout() {
    this.clearAuthData();
    this.router.navigate(['/']);
  }

  autoAuth() {
    const authInfo = this.getAuthData();
    if (!authInfo) return;
    this.token = authInfo.token;
    if (authInfo.expirationDate) {
      const now = new Date();
      const expiresIn = authInfo.expirationDate.getTime() - now.getTime();
      this.setAuthTimer(expiresIn/1000);
    }
  }

  private saveAuthData(token, expirationDate) {
    localStorage.setItem('tokenBI', token);
    localStorage.setItem('expirationBI', expirationDate.toISOString());
  }

  private clearAuthData() {
    this.token = undefined;
    localStorage.removeItem('tokenBI');
    localStorage.removeItem('expirationBI');
  }

  private getAuthData() {
    const token = localStorage.getItem('tokenBI');
    const expirationDate = localStorage.getItem('expirationBI');
    if (!token) {
        return;
    }
    return { token, expirationDate: new Date(expirationDate) };
  }

  private setAuthTimer(duration: number) {
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 1000);
  }

}
