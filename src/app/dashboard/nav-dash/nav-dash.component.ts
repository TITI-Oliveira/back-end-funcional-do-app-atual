import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
declare var $:any;

@Component({
  selector: 'nav-dash',
  templateUrl: './nav-dash.component.html',
  styleUrls: ['./nav-dash.component.css']
})
export class NavDashComponent implements OnInit {

  constructor(private userService: UserService) { }
  
  ngOnInit() {
    $('.tabs').tabs();
    $('.sidenav').sidenav();
  }

  onLogout() {
    this.userService.logout();
  }

}
