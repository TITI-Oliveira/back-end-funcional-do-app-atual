import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { MatExpansionModule } from "@angular/material";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppComponent } from './app.component';
import { IntroHomeComponent } from './home/intro-home/intro-home.component';
import { NavHomeComponent } from './home/nav-home/nav-home.component';
import { HomeComponent } from './home/home.component';
import { Bring2meComponent } from './core/bring2me/bring2me.component';
import { Bring2someoneComponent } from './core/bring2someone/bring2someone.component';
import { EntregarComponent } from './core/bring2someone/entregar/entregar.component';
import { FeedComponent } from './core/bring2someone/feed/feed.component';
import { AbertosComponent } from './core/bring2me/abertos/abertos.component';
import { ConcluidosComponent } from './core/bring2me/concluidos/concluidos.component';
import { EnviadosComponent } from './core/bring2me/enviados/enviados.component';
import { CadastroComponent } from './home/cadastro/cadastro.component';
import { LoginComponent } from './home/login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PerfilComponent } from './core/perfil/perfil.component';
import { NavPerfilComponent } from './core/perfil/nav-perfil/nav-perfil.component';
import { PedidoComponent } from './core/pedido/pedido.component';
import { NavBring2meComponent } from './core/bring2me/nav-bring2me/nav-bring2me.component';
import { NavDashComponent } from './dashboard/nav-dash/nav-dash.component';
import { AppRoutingModule } from './routes.module';
import { DetalhesComponent } from './core/bring2me/abertos/detalhes/detalhes.component';
import { AuthInterceptor } from './interceptor/auth.interceptor';
import { AdmloginComponent } from './admin/admlogin/admlogin.component';
import { AdmdashboardComponent } from './admin/admdashboard/admdashboard.component';
import { AdmheaderComponent } from './admin/admheader/admheader.component';
import { UserComponent } from './admin/admreports/user/user.component';
import { OrderComponent } from './admin/admreports/order/order.component';

@NgModule({
  declarations: [
    AppComponent,
    IntroHomeComponent,
    NavHomeComponent,
    HomeComponent,
    Bring2meComponent,
    Bring2someoneComponent,
    EntregarComponent,
    FeedComponent,
    AbertosComponent,
    ConcluidosComponent,
    EnviadosComponent,
    CadastroComponent,
    LoginComponent,
    DashboardComponent,
    PerfilComponent,
    NavPerfilComponent,
    PedidoComponent,
    NavBring2meComponent,
    NavDashComponent,
    DetalhesComponent,
    AdmloginComponent,
    AdmdashboardComponent,
    AdmheaderComponent,
    UserComponent,
    OrderComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    MatExpansionModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
