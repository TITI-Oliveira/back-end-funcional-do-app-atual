import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../services/admin.service';
declare var $:any;

@Component({
  selector: 'admin-header',
  templateUrl: './admheader.component.html',
  styleUrls: ['./admheader.component.css']
})
export class AdmheaderComponent implements OnInit {

  constructor(private admService: AdminService) { }

  ngOnInit() {
    $('.dropdown-trigger').dropdown();
  }

  logout() {
    this.admService.logout();
  }

}
