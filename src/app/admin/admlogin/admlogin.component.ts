import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AdminService } from '../../services/admin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admlogin',
  templateUrl: './admlogin.component.html',
  styleUrls: ['./admlogin.component.css']
})
export class AdmloginComponent implements OnInit {
  admLoginForm: FormGroup;

  constructor(private adminService: AdminService, private router: Router) { }

  ngOnInit() {
    this.admLoginForm = new FormGroup({
      'email': new FormControl(
        null,
        { validators: [Validators.required, Validators.email] }
      ),
      'password': new FormControl(
        null,
        { validators: [Validators.required] }
      )
    });
  }

  get email() {
    return this.admLoginForm.get('email');
  }

  get password() {
    return this.admLoginForm.get('password');
  }

  onLogin() {
    if (this.admLoginForm.invalid) return;
    this.adminService.login({ ...this.admLoginForm.value }).then(response => {
      if (response) this.router.navigate(['/dashboard'])
    });
  }

}
