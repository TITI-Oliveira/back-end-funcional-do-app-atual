import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../services/admin.service';

@Component({
  selector: 'app-admdashboard',
  templateUrl: './admdashboard.component.html',
  styleUrls: ['./admdashboard.component.css']
})
export class AdmdashboardComponent implements OnInit {
  ordersSum;
  usersSum;
  
  constructor(private adminService: AdminService) { }

  ngOnInit() {
    this.adminService.getOrdersFromLastWeek().then(sum => this.ordersSum = sum);
    this.adminService.getUsersFromLastWeek().then(sum => this.usersSum = sum);
  }

}
