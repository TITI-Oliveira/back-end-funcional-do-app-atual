import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AdminService } from '../../../services/admin.service';

declare var $:any;

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  searchForm: FormGroup;
  searchByProductForm: FormGroup;
  results;
  hasResults = false;

  constructor(private adminService: AdminService) { }

  ngOnInit() {
    $('.datepicker').datepicker();
    $('#dataInicial').datepicker({
      onSelect: (time) => {
        console.log(time);
        this.initialDate.setValue(time);
      }
    });
    $('#dataFinal').datepicker({
      onSelect: (time) => {
        console.log(time);
        this.finalDate.setValue(time);
      }
    });
    this.searchForm = new FormGroup({
      'initialDate': new FormControl(
        null,
        { validators: [Validators.required] }
      ),
      'finalDate': new FormControl(
        null,
        { validators: [Validators.required] }
      )
    });
    this.searchByProductForm = new FormGroup({
      'productName': new FormControl(
        null,
        { validators: [Validators.required] }
      )
    })
  }

  get initialDate() {
    return this.searchForm.get('initialDate');
  }

  get finalDate() {
    return this.searchForm.get('finalDate');
  }

  onToggleProductRelatory() {
    $('main').toggle(500);
    $('#productRelatory').toggle(500);
  }

  onTogglePeriodRelatory() {
    $('main').toggle(500);
    $('#periodRelatory').toggle(500);
  }

  onSubmit() {
    console.log(this.searchForm.value);
    this.adminService.searchOrders({ ...this.searchForm.value }).then(orders => {
      console.log(orders);
      this.results = orders;
      if(this.results) {
        this.hasResults = true;
      } else {
        this.hasResults = false;
      }
    })
    .catch(err => console.log(err));
    // console.log(this.searchForm.value);
    // let date = new Date($('#dataInicial').val());
    // console.log(date.toISOString());
  }

  onSubmitByProduct() {
    console.log(this.searchForm.value);
    this.adminService.searchOrdersByProductName({ ...this.searchByProductForm.value }).then(orders => {
      console.log(orders);
      this.results = orders;
      if(this.results) {
        this.hasResults = true;
      } else {
        this.hasResults = false;
      }
    })
    .catch(err => console.log(err));
    // console.log(this.searchForm.value);
    // let date = new Date($('#dataInicial').val());
    // console.log(date.toISOString());
  }

}
