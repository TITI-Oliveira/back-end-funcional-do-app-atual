import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AdminService } from '../../../services/admin.service';
declare var $:any;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  searchForm: FormGroup;
  results;
  hasResults = false;

  constructor(private adminService: AdminService) { }

  ngOnInit() {
    $('.datepicker').datepicker();
    $('#dataInicial').datepicker({
      onSelect: (time) => {
        this.initialDate.setValue(time);
      }
    });
    $('#dataFinal').datepicker({
      onSelect: (time) => {
        this.finalDate.setValue(time);
      }
    });
    this.searchForm = new FormGroup({
      'initialDate': new FormControl(
        null,
        { validators: [Validators.required] }
      ),
      'finalDate': new FormControl(
        null,
        { validators: [Validators.required] }
      )
    });
  }

  get initialDate() {
    return this.searchForm.get('initialDate');
  }

  get finalDate() {
    return this.searchForm.get('finalDate');
  }

  onSubmit() {
    this.adminService.searchUsers({ ...this.searchForm.value }).then(users => {
      this.results = users;
      if(this.results) {
        this.hasResults = true;
      } else {
        this.hasResults = false;
      }
    })
    .catch(err => console.log(err));
    // console.log(this.searchForm.value);
    // let date = new Date($('#dataInicial').val());
    // console.log(date.toISOString());
  }

}
