const mongoose = require('mongoose');

const adminSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  function: {
    type: String,
    default: null
  },
  email: {
    type: String,
    required: true,
    index: { unique: true }
  },
  password: {
    type: String,
    required: true
  },
  profile_image: {
    type: String
  }
});

module.exports = mongoose.model('Admin', adminSchema);