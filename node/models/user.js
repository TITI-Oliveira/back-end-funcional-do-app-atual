const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    index: { unique: true }
  },
  password: {
    type: String,
    required: true
  },
  profile_image: {
    type: String
  },
  created_on: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('User', userSchema);