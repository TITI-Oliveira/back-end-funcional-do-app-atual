const mongoose = require('mongoose');

const orderSchema = mongoose.Schema({
  product: {
    type: String,
    required: true,
    trim: true,
    minlength: 1
  },
  price: {
    type: Number,
    required: true
  },
  reward_value: {
    type: Number,
    required: true
  },
  location_from: {
    type: String,
    required: true,
    trim: true,
    minlength: 1
  },
  location_final: {
    type: String,
    required: true,
    trim: true,
    minlength: 1
  },
  status: {
    type: String,
    default: 1
  },
  buyer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
  proposals: {
    type: Array,
    default: null
  },
  bringer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    default: null
  },
  arrival_expectation: {
    type: Date,
    default: null
  },
  arrival_date: {
    type: Date,
    default: null
  },
  created_on: {
    type: Date,
    default: Date.now
  }
  
});

module.exports = mongoose.model('Order', orderSchema);