const mongoose = require('mongoose');

const founderSchema = mongoose.Schema({
  nome: {
    type: String,
    required: true
  },
  funcao: {
    type: String,
    required: true
  },
  profile_image: {
    type: String
  }
});

module.exports = mongoose.model('Founder', founderSchema);