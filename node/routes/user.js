const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const multer = require('multer');

const checkAuth = require('../middleware/auth-validate');

const User = require('../models/user');

// router.post('/signup', (req, res, next) => {
//   bcrypt.hash(req.body.password, 12).then(hash => {
//     const user = new User({
//       nome: req.body.nome,
//       email: req.body.email,
//       password: hash
//     });
//     console.log(user);
//     user.save()
//     .then(result => {
//       res.status(201).json({ message: 'User created successfully' });
//     })
//     .catch(error => {
//       console.log(error);
//       res.status(500).json({
//         message: 'An unexpected error has ocurred',
//         error
//       });
//     });
//   });
// });

const MIME_TYPE_MAP = {
  'image/png': 'png',
  'image/jpeg': 'jpg',
  'image/jpg': 'jpg'
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const isValid = MIME_TYPE_MAP[file.mimetype];
    let error = new Error("Invalid mime type");
    if (isValid) {
      error = null;
    }
    cb(error, "node/images");
  },
  filename: (req, file, cb) => {
    const name = file.originalname.toLowerCase().split(' ').join('-');
    const ext = MIME_TYPE_MAP[file.mimetype];
    console.log(name);
    cb(null, name + '-' + Date.now() + '.' + ext);
  }
});

const mapUser = (user) => {
  return {
    id: user._id,
    name: user.name,
    email: user.email
  };
}

router.get('/getProfile', checkAuth, (req, res, next) => {
  User.find({ _id: req.userData.id }).then(response => {
    user = mapUser(response[0]);
    res.status(200).json({ message: 'User was found successfully', user })
  });
});

router.post('/signup', (req, res, next) => {
  bcrypt.hash(req.body.password, 12).then(hash => {
    const user = new User({
      name: req.body.name,
      email: req.body.email,
      password: hash
    });
    user.save()
    .then(result => {
      res.status(201).json({ message: 'User created successfully' });
    })
    .catch(error => {
      res.status(500).json({
        message: 'An unexpected error has ocurred',
        error
      });
    });
  });
});

router.post('/login', (req, res, next) => {
  let user;
  User.findOne({ email: req.body.email })
    .then(data => {
      if (!data) {
        return res.status(401).json({ message: 'User cannot be authenticated' });
      }
      user = data;
      return bcrypt.compare(req.body.password, user.password)
    })
    .then(result => {
      if (!result) {
        return res.status(401).json({ message: 'User cannot be authenticated' });
      }
      if (!req.body.keepLogged) {
        const token = jwt.sign(
          { id: user._id, name: user.name },
          'brthlsfrtnct',
          { expiresIn: '3h' }
        );
        res.status(200).json({ token, expiresIn: 10800 });
      }
      const token = jwt.sign(
        { id: user._id, name: user.name },
        'brthlsfrtnct'
      );
      res.status(200).json({ token });
    })
    .catch(err => {
      return res.status(401).json({ message: 'User cannot be authenticated' });
    });
});

router.put('/updateProfile', checkAuth, (req, res, next) => {
  // console.log(req.file);
  const profile = new User({
    _id: req.userData.id,
    ...req.body
  });
  User.updateOne({ _id: req.userData.id }, profile).then(response => console.log(response));
  res.status(200).json({ message: 'User profile updated successfully' });
});

// router.put('/updateProfile', checkAuth, (req, res, next) => {
//   console.log('PITO');
//   const profile = new User({
//     _id: req.userData.id,
//     ...req.body
//   });
//   console.log(profile);
// });

module.exports = router