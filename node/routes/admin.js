const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const dateFormat = require('dateformat');

const checkAuth = require('../middleware/auth-validate');

const Admin = require('../models/admin');
const Order = require('../models/order');
const User = require('../models/user');

const getUserName = (username) => {
  let response;
  response = User.findOne({ _id: username }).then(user => user.name);
  console.log(response);
  response.then(final => console.log(final));
}

const getLastWeekDate = () => {
  var today = new Date();
  var lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
  return lastWeek ;
}

const getOrganizedOrderArray = async (orders) => {
  let response = await orders.map(order => {
    return {
      id: order._id,
      product: order.product,
      price: order.price,
      buyer: (async () => await getUserName(order.buyer)),
      reward_value: order.reward_value,
      created_on: order.created_on 
    }
  });
  // console.log(response);
  return response;
}

router.get('/sum/orders', (req, res, next) => {
  Order.find({ created_on: { '$gte': getLastWeekDate() } }).count().then(ordersSum => res.status(200).json({ ordersSum }));
});

router.get('/sum/users', (req, res, next) => {
  User.find({ created_on: { '$gte': getLastWeekDate() } }).count().then(usersSum => res.status(200).json({ usersSum }));
});

router.post('/login', (req, res, next) => {
  // console.log(req.body);
  let user;
  Admin.findOne({ email: req.body.email })
  .then(response => {
    if (!response) return res.status(401).json({ message: 'Authentication failed' })
    user = response;
    // console.log(response);
    return bcrypt.compare(req.body.password, user.password)
  })
  .then(result => {
    if (!result) return res.status(401).json({ message: 'Authentication failed' });
    const token = jwt.sign(
      { id: user._id, name: user.name },
      'tltibrfrsp',
      { expiresIn: '6h' }
    );
    res.status(200).json({ message: 'Authentication succeeded', token, expiresIn: 21600 });
  })
  .catch(err => {
    console.log(err);
    res.status(401).json({ message: 'Authentication failed' });
  });
  // bcrypt.compare(req.body.password, user.password).then()
});

router.post('/reports/users', (req, res, next) => {
  console.log(req.body);
  User.find({ created_on: { '$gte': req.body.initialDate, '$lte': req.body.finalDate } }).then(users => {
    console.log(users);
    if (users) {
      users = users.map(user => {
        return {
          id: user._id,
          name: user.name,
          email: user.email,
          // created_on: new Date(user.created_on)
          created_on: dateFormat(user.created_on, "dd/mm/yyyy HH:MM")
        }
      });
      res.status(200).json({message: 'Users were found between the specified dates', users});
    }
    res.status(404).json({message: 'Oops'});
  });
});

router.post('/reports/orders', (req, res, next) => {
      // // console.log(orders);
    // if (orders) {
    //   orders = await orders.map(async (user) => {
    //     // userName = User.findOne({ _id: user.buyer }).select('name').then(name => {
    //     //   // console.log(name.name);
    //     //   return name.name;
    //     // });
    //     // console.log(userName);
    //     let userName = await getUserName(user.buyer);
    //     console.log(userName);
    //     // let userName;
    //     // User.findOne({ _id: user.buyer }).then(result => userName = result.name);
    //     return {
    //       id: user._id,
    //       product: user.product,
    //       buyer: userName,
    //       price: user.price,
    //       reward_value: user.reward_value,
    //       // created_on: new Date(user.created_on)
    //       created_on: dateFormat(user.created_on, "dd/mm/yyyy HH:MM")
    //     }
    //     // console.log(userName);
    //     // console.log(userName);
    //   });
    //   console.log(orders);

  // console.log(req.body);
  Order.find({ created_on: { '$gte': req.body.initialDate, '$lte': req.body.finalDate } }).populate({ path: "buyer", select: "name" }).exec((err, orders) => {
      orders = orders.map(order => {
        return {
          id: order._id,
          product: order.product,
          price: order.price,
          reward_value: order.reward_value,
          buyer: order.buyer.name,
          location_from: order.location_from,
          location_final: order.location_final,
          created_on: dateFormat(order.created_on, "dd/mm/yyyy HH:MM")
        }
      });

      res.status(200).json({ message: 'Orders were found between the selected period', orders });
  })
});

router.post('/reports/ordersByProduct', (req, res, next) => {
  console.log(req.body)
  Order.find({ product: new RegExp(req.body.productName, 'i') }).populate({ path: "buyer", select: "name" }).exec((err, orders) => {
    // console.log
    orders = orders.map(order => {
      return {
        id: order._id,
        product: order.product,
        price: order.price,
        reward_value: order.reward_value,
        buyer: order.buyer.name,
        location_from: order.location_from,
        location_final: order.location_final,
        created_on: dateFormat(order.created_on, "dd/mm/yyyy HH:MM")
      }
    });

    res.status(200).json({ message: 'Orders were found between the selected period', orders });
  });
});

// router.post('/create', (req, res, next) => {
//   bcrypt.hash(req.body.password, 14).then(hash => {
//     const admin = new Admin({
//       name: req.body.name,
//       email: req.body.email,
//       password: hash
//     });
//     admin.save().then(message => console.log(message));
//   });
// });

module.exports = router