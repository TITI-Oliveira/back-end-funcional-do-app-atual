const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const checkAuth = require('../middleware/auth-validate');

const Order = require('../models/order');
const User = require('../models/user');

// router.get('/:id/bring2me', checkAuth, (req, res, next) => {
//   Order.find({ buyer: req.params.id })
//     .then(orders => {
//       res.status(200).json({
//         message: 'Orders were found!',
//         orders
//       });
//     });
// });

const mapOrders = (orders) => {
  return orders.map(order => {
    return {
      id: order._id,
      product: order.product,
      price: order.price,
      reward_value: order.reward_value,
      proposals: order.proposals,
      location_from: order.location_from,
      location_final: order.location_final
    }
  });
};

const mapOrder = (order) => {
  return {
    id: order._id,
    product: order.product,
    price: order.price,
    reward_value: order.reward_value,
    proposals: order.proposals,
    location_from: order.location_from,
    location_final: order.location_final,
    status: order.status
  };
}

router.get('/getAll', checkAuth, (req, res, next) => {
  Order.find({ status: '1' })
    .nor([{ buyer: req.userData.id }, { bringer: req.userData.id }])
    .then(orders => {
      orders = mapOrders(orders);
      res.status(200).json({ orders });
    });
})

router.get('/getById/:id', checkAuth, (req, res, next) => {
  Order.findOne({ _id: req.params.id, buyer: req.userData.id })
    .then(order => {
      order = mapOrder(order);
      res.status(200).json({ order });
    })
})

router.get('/getByStatus/:status', checkAuth, (req, res, next) => {
  // console.log(req.userData);
  Order.find({ status: req.params.status, buyer: req.userData.id })
    .then(orders => {
      orders = mapOrders(orders);
      res.status(200).json({ orders });
    })
});

router.get('/getByStatus/:status/bringer', checkAuth, (req, res, next) => {
  Order.find({ status: req.params.status, bringer: req.userData.id })
    .then(orders => {
      orders = mapOrders(orders);
      res.status(200).json({ orders });
    })
});

router.put('/edit/:id', checkAuth, (req, res, next) => {
  // console.log(req.body);
  let order = new Order({
    ...req.body,
    buyer: req.userData.id
  });
  Order.updateOne({ _id: req.params.id, buyer: req.userData.id }, order).then(result => {
    order = mapOrder(order);
    res.status(200).json({ order });
  });
  // Order.find({ _id: req.params.id, buyer: req.userData.id }).then(response => console.log(response));
  // order.save().then((savedOrder) => {
  //   savedOrder = mapOrder(savedOrder);
  //   res.status(201).json({ message: 'Order successfully created', order: savedOrder });
  // });
});

router.put('/:order/addProposal', checkAuth, (req, res, next) => {
  // console.log(req.body);
  User.find({ _id: req.userData.id }).select('name -_id')
    .then(response => {
      name = response[0].name
      req.body.order.proposals.push({ id: new mongoose.mongo.ObjectID(), value: req.body.offer, seller_id: req.userData.id , seller_name: name})
      const order = new Order({
        _id: req.body.order.id,
        ...req.body.order
      });
      Order.updateOne({ _id: order._id }, order).then(result => {
        res.status(200).json({ message: 'Proposal was made successfully' });
      });
    });
});

router.post('/create', checkAuth, (req, res, next) => {
  // console.log(req.body);
  const order = new Order({
    ...req.body,
    buyer: req.userData.id
  });
  order.save().then((savedOrder) => {
    savedOrder = mapOrder(savedOrder);
    res.status(201).json({ message: 'Order successfully created', order: savedOrder });
  });
});

router.delete('/delete/:id', checkAuth, (req, res, next) => {
  Order.deleteOne({ _id: req.params.id, buyer: req.userData.id }).then(response => {
    res.status(200).json({ message: 'Order successfully deleted'});
  });
});

// router.put('/edit/:id', checkAuth, (req, res, next) => {
//   console.log("CUUUUUUUUUUUUUUUUUUUUUUUU");
//   const order = new Order({
//     ...req.body,
//     buyer: req.userData.id
//   });
//   Order.updateOne({ _id: req.params.id, buyer: req.userData.id }, order).then(response => {
//     console.log(response);
//   });
// });

// router.put('/edit', checkAuth, (req, res, next) => {
//   // console.log(req.body);
//   console.log('KEKEKEKEKEKEKEKEKEK');
//   // const order = new Order({
//   //   ...req.body
//   // });
//   // console.log(order);
// });

// router.post('', checkAuth, (req,res,next) => {
//   const order = new Order({
//     product: req.body.product,
//     price: req.body.price,
//     reward_value: req.body.reward_value,
//     location_from: req.body.location_from,
//     location_final: req.body.location_final,
//     buyer: 'Teste'
//   });
//   order.save().then(() => {
//     res.status(201).json({
//       message: 'Pedido feito com sucesso'
//     });
//   });
// });

// router.get('', checkAuth, (req,res,next) => {
//   Order.find().then(orders => {
//     res.status(200).json({
//       message: 'Fundadores encontrados com sucesso',
//       orders
//     });
//   });
// });

// router.get('/b2s/:bringer', checkAuth, (req,res,next) => {
//   Order.find({bringer: req.params.bringer}).then(orders => {
//     res.status(200).json({
//       message: 'Pedidos encontrados com sucesso',
//       orders
//     });
//   });
// });

// router.patch('/edit/:id', (req,res,next) => {
//   console.log(req.body);
//   console.log(req.params.id);
//   // console.log(order);
//   Order.findByIdAndUpdate(req.params.id, {
//     $set: {
//       product: req.body.product,
//       price: req.body.price,
//       reward_value: req.body.reward_value,
//       location_from: req.body.location_from,
//       location_final: req.body.location_final,
//       buyer: 'Teste'} 
//   },{new: true}).then(console.log("KEK"));
// });

// router.delete('/delete/:id', (req,res,next) => {
//   console.log('KEKO');
//   console.log(req.params.id);
//   Order.findByIdAndRemove(req.params.id).then((order) => {
//     console.log(order);
//   });
// });

// router.get('/details/:_id', (req,res,next) => {
//   Order.find({_id: req.params._id}).then(orders => {
//     res.status(200).json({
//       message: 'Pedidos encontrados com sucesso',
//       orders
//     });
//   });
// });

// router.get('/:status', (req,res,next) => {
//   Order.find({status: req.params.status}).then(orders => {
//     res.status(200).json({
//       message: 'Pedidos encontrados com sucesso',
//       orders
//     });
//   });
// });

module.exports = router