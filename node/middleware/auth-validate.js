const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  try {
    console.log(req.header.authorization);
    const token = req.headers.authorization.split(" ")[1];
    const decodedToken = jwt.verify(token, 'brthlsfrtnct');
    req.userData = { ...decodedToken };
    next();
  } catch (error) {
    res.status(401).json({ message: 'Auth failed', error });
  }
};