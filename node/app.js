const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const orderRoutes = require('./routes/order');
const userRoutes = require('./routes/user');
const adminRoutes = require('./routes/admin');

const Founder = require('./models/founder');

const app = express();

mongoose.connect('mongodb://localhost:27017/bring2me')
  .then(() => {
    console.log('Connected to database!');
  })
  .catch((e) => {
    console.log('Connection failed', e);
  });

app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({extended: false}));

app.use((req,res,next) => {
  res.setHeader('Access-Control-Allow-Origin', "*");
  res.setHeader(
    'Access-Control-Allow-Headers',
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    'Access-Control-Allow-Methods',
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});

app.get('/api/founders', (req,res,next) => {
  Founder.find().then(founders => {
    res.status(200).json({
      message: 'Fundadores encontrados com sucesso',
      founders
    });
  });
});

app.use("/api/order", orderRoutes);
app.use("/api/user", userRoutes);
// app.use("/api/admin", adminRoutes);
app.use("/api/admin", adminRoutes);

module.exports = app;